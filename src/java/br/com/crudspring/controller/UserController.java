 
package br.com.crudspring.controller;
 
import br.com.crudspring.connection.Connection;
import br.com.crudspring.model.User;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


public class UserController {
    Connection conn = new Connection();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(conn.Connect());
    ModelAndView mav = new ModelAndView();
    int id;
    String qry;
    List data;
    
    
    @RequestMapping("index.htm")
    public ModelAndView Index(){
        qry = "SELECT * FROM USER";
        data = this.jdbcTemplate.queryForList(qry);
        mav.addObject("listUser", data);
        mav.setViewName("index");
        return mav;
    }
    
    @RequestMapping(value = "create.htm", method = RequestMethod.GET)
    public ModelAndView Create(){
        mav.addObject(new User());
        mav.setViewName("create");
        return mav;
    }
    @RequestMapping(value ="create.htm", method = RequestMethod.POST)
    public ModelAndView Create (User us){
        qry = "INSERT INTO USER (name, city, street, state) VALUES(?,?,?,?)";
        this.jdbcTemplate.update(qry, us.getName(), us.getCity(), us.getStreet(), us.getState());
        return new ModelAndView("redirect://index.htm");
    }
    
    @RequestMapping(value = "edit.htm", method = RequestMethod.GET)
    public ModelAndView Edit(HttpServletRequest request){
        id=Integer.parseInt(request.getParameter("id"));
        qry = "SELECT * FROM USER WHERE Id = " +id;
        data = this.jdbcTemplate.queryForList(qry);
        mav.addObject("listUser", data);
        mav.setViewName("edit");
        return mav;
    }
    @RequestMapping(value = "edit.htm", method = RequestMethod.POST)
    public ModelAndView Edit(User us){
        qry = "UPDATE USER SET Name=?, City=?, Street=?, State=? WHERE Id = " + id;
        this.jdbcTemplate.update(qry, us.getName(), us.getCity(), us.getStreet(), us.getState());
        return new ModelAndView("redirect://index.htm");
    }
    
    @RequestMapping("delete.htm")
    public ModelAndView Delete(HttpServletRequest request){
        id=Integer.parseInt(request.getParameter("id"));
        qry = "DELETE FROM USER WHERE Id = " +id;
        this.jdbcTemplate.update(qry);
        return new ModelAndView("redirect://index.htm");
    }
}

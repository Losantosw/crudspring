

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Crud Spring</title>
    </head>
    <body>
        <div class="container mt-4">
            <div class="card border-info">
                <div class="card-header bg-info text-white">
                    <h4>Editar Registro</h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <label>Nome</label>
                        <input type="text" name="name" class="form-control" value="${listUser[0].name}"/>

                        <label>Cidade</label>
                        <input type="text" name="city" class="form-control" value="${listUser[0].city}"/>

                        <label>Bairro</label>
                        <input type="text" name="street" class="form-control" value="${listUser[0].street}"/>

                        <label>Estado</label>
                        <input type="text" name="state" class="form-control" value="${listUser[0].state}"/>
                        
                        <br>
                        
                        <input type="submit" value="Atualizar" class="btn btn-success">
                        <a href="index.htm" class="btn btn-info">Voltar</a>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

